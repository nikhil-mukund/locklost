locklost: aLIGO IFO lock loss tracking and analysis tool
========================================================

This primarily provides a command line interface for:

* searching times for lock loss events using guardian transitions
* refining event times using cavity signals
* manage and view lockloss events

Condor DAQ jobs can be generated for searching for and refining
events.

Uses GWpy or NDS to retrieve data.

## Usage:

Search for lock losses within some time window:

```
python -m locklost search gps_start gps_end
```

Run an analysis on times found by running a search:

```
python -m locklost analyze gps_time
```

## Installation:

1. If you're installing this from source:

```
python setup.py install --prefix=path/to/install/dir
```

Make sure to update your PYTHONPATH to point to the install directory.

2. conda/pip:

Go to the top-level directory of locklost, then run:

```
pip install .
```

Alternative, you can run:

```
pip install git+https://git.ligo.org/jameson.rollins/locklost.git
```
