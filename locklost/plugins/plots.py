import os
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
import logging

from gwpy.segments import Segment
import pydv

from . import config
from .event import LocklossEvent
from . import data


def plot_channel(args):
    gt, channel, outdir = args

    logging.info("plotting: %s" % channel)

    outfile = '%s__WIDE.png' % (channel)
    window = config.PLOT_WINDOWS['WIDE']

    gps = int(gt.gps())
    xmin = gps + window[0]
    xmax = gps + window[1]

    fig = pydv.plot(
        channel_names=[[channel]],
        center_time=gps,
        xmin=xmin,
        xmax=xmax,
        title='',
        sharex=True,
        sharey=False,
        no_threshold=True,
        no_wd_state=True,
        save=None,
        show=False,
        )

    fig.texts[0].set_text('%s' % channel)
    fig.subplots_adjust(left=0.15, right=0.95, top=0.85)
    fig.set_size_inches(
        config.FIG_SIZE[0], config.FIG_SIZE[1],
        bbox_inches='tight', forward=True)

    fig.savefig(os.path.join(outdir, outfile))

    outfile = '%s__ZOOM.png' % (channel)
    window = config.PLOT_WINDOWS['ZOOM']
    ax = fig.get_axes()[0]
    ax.set_autoscaley_on(True)
    ax.set_xlim(*window)
    # reset y limites
    data = ax.get_lines()[0].get_xydata()
    x = data[:,0]
    y = data[:,1]
    ind = np.where(np.logical_and(x>window[0], x<window[1]))
    ylim = np.min(y[ind]), np.max(y[ind])
    if ylim != (-0, -0):
        ax.set_ylim(*ylim)

    fig.savefig(os.path.join(outdir, outfile))


def gen_plots(channels, gt, outdir):
    args = [(gt, chan, outdir) for chan in channels]
    #args = itertools.izip(channels, itertools.repeat(gt), itertools.repeat(outdir))
    # for a in args:
    #     plot_channel(a)
    # return
    pool = multiprocessing.Pool(5)
    pool.map(plot_channel, args)
    pool.close()
    pool.join()
