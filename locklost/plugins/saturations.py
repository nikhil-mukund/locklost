
import os
import sys
import argparse
import logging
import signal
from math import isnan

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault

from gwpy.segments import Segment

from .. import config
from ..event import LocklossEvent, gen_events
from .. import data

#################################################

def find_saturations(event):
    """
    Create plot/list of saturating suspension channels before refined lockloss
    time (with corresponding gps times for saturations).
    """

    saturation_threshold = config.SATURATION_THRESHOLD
    high_thresh_channels = config.ETMX_L3_CHANNELS
    plot_first = config.PLOT_SATURATIONS
    sat_search_window = config.SAT_SEARCH_WINDOW
    gps = int(event.gps)
    refined_gps = event.refined_gps
    if isnan(refined_gps) or not refined_gps:
        logging.warning("saturations being compiled for unrefined lockloss event time")
        refined_gps = gps

    channels = gen_channel_names()
    bufs, segment = fetch_data(channels, refined_gps)

    #checks for saturations within the first half of the search window,
    #and extends the search window if there are any
    expand = True
    num_expand = 0
    shift = -0.5*sat_search_window[0]
    while expand is True:
        for idx, buf in enumerate(bufs):
            channel_name = str(buf.channel)
            if refined_gps > config.ETMX_L3_CHANGE_DATE and channel_name in high_thresh_channels:
                buf.data = buf.data/4
            srate = buf.sample_rate
            early_thresh = shift*srate
            if any(abs(buf.data[:early_thresh]) >= saturation_threshold*0.9):
                bufs, segment = fetch_data(channels, refined_gps, move_left=(num_expand+1)*shift)
                num_expand += 1
                break
            else:
                expand = False
        if num_expand == 6:
            break

    #checks for any channels saturating before refined gps time and logs channel
    #name, saturations time, full time series, and full data series

    saturations = []
    for buf in bufs:
        channel_name = str(buf.channel)
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        newtime_idx = max(np.nonzero(t <= refined_gps)[0])
        sat_idxs = np.nonzero(abs(buf.data[:newtime_idx]) >= saturation_threshold)[0]
        if len(sat_idxs) > 0:
            saturations.append([channel_name, t[min(sat_idxs)], t, buf.data])

    if not saturations:
        logging.info("No saturated suspension channels.")

    else:
        logging.info("Saturated SUS channels before lockloss: "+str(len(saturations)))

        saturations.sort(key=lambda x: x[1])
        first_sat = saturations[0][1] - refined_gps
        rev_sat = list(reversed(saturations[0:plot_first]))

        # set plot metaparams
        plt.rcParams.update(rcParamsDefault)
        plt.rcParams['font.size'] = 30.0
        plt.rcParams['axes.titlesize'] = 30.0
        plt.rcParams['xtick.labelsize'] = 30.0
        plt.rcParams['ytick.labelsize'] = 30.0
        plt.rcParams['legend.fontsize'] = 30.0
        plt.rcParams['agg.path.chunksize'] = 1000

        zoomed_window = [first_sat-5, first_sat+1]
        if first_sat - (segment[0] - refined_gps) > 40:
            wide_window = [first_sat - 40, segment[1] - refined_gps]
        else:
            wide_window = [segment[0] - refined_gps, segment[1] - refined_gps]
        windows = [wide_window, zoomed_window]
        zoom_levels = ['WIDE', 'ZOOM']
        ymin, ymax = set_ylims(refined_gps, saturations)
        for zoom_level, window in zip(zoom_levels, windows):

            fig, ax = plt.subplots(1, figsize=(22,16))
            ax.set_xlabel('Time [s] since refined lock loss at {}'.format(refined_gps), labelpad=10)
            ax.set_ylabel('Fraction of saturation threshold')
            ax.set_xlim(*window)
            ax.set_ylim(ymin, ymax)
            ax.grid()
            for idx, val in enumerate(rev_sat):
                name = distill_channel_name(val[0])
                ax.plot(
                    val[2]-refined_gps,
                    val[3]/saturation_threshold,
                    color = config.SATURATION_CM[idx],
                    label = distill_channel_name(val[0]),
                    alpha=0.8,
                    lw=2,
                )
            handles, labels = ax.get_legend_handles_labels()

            ax.axvline(
                first_sat,
                color = 'black',
                linestyle = '--',
                lw = 3,
            )
            ax.annotate(
                'First Saturation: '+str(first_sat+refined_gps),
                xy=(first_sat, ax.get_ylim()[1]),
                xycoords='data',
                xytext=(0, 2),
                textcoords='offset points',
                horizontalalignment='center',
                verticalalignment='bottom',
                bbox=dict(boxstyle="round", fc="w", ec="black", alpha=0.95),
                )

            # draw lines on plot for saturation thresholds, lock loss time
            ax.axhline(
                1,
                color = 'red',
                linestyle = '--',
                lw = 10
            )
            ax.axhline(
                -1,
                color = 'red',
                linestyle = '--',
                lw = 10
            )
            ax.axvline(
                0,
                color = 'black',
                linestyle = '-',
                lw = 2
            )

            lgd = ax.legend(handles[::-1], labels[::-1], bbox_to_anchor=(1.1, 0.96), title='optic/stage/corner')
            fig.tight_layout(pad = 0.05)
            ax.set_title('Saturating suspension channels (ordered by time of saturation)', y=1.04)

            #saves plot to lockloss directory
            outfile_plot = 'saturations_{}.png'.format(zoom_level)
            outpath_plot = os.path.join(event.path, outfile_plot)
            fig.savefig(outpath_plot, bbox_inches='tight')
            fig.clf()

        #saves saturating channel names/times to lockloss directory
        outfile_csv = 'saturations.csv'
        outpath_csv = os.path.join(event.path, outfile_csv)
        with open(outpath_csv, 'wb') as myfile:
            for i in saturations:
                myfile.write('%s %f\n' % (i[0], i[1]))


def set_ylims(refined_gps, saturations):
    """
    Calculate ylims for the section of SUS channels occuring before refined/
    unrefined lockloss time.
    """

    ymaxs = []
    ymins = []
    for sat in saturations:
        t, y = sat[2], sat[3]
        scaling_times = np.where(t < refined_gps-5)[0]
        scaling_vals = y[scaling_times]
        ymins.append(min(scaling_vals))
        ymaxs.append(max(scaling_vals))

    if max(ymaxs) > 0:
        ymax = 1.1*max(ymaxs)
    elif max(ymaxs) < 0:
        ymax = 0.9*max(ymaxs)
    if min(ymins) < 0:
        ymin = 1.1*min(ymins)
    elif min(ymins) > 0:
        ymin = 0.9*min(ymins)

    if ymin > -config.SATURATION_THRESHOLD:
        ymin = -config.SATURATION_THRESHOLD - 10000
    if ymax < config.SATURATION_THRESHOLD:
        ymax = config.SATURATION_THRESHOLD + 10000

    ymin = float(ymin)/config.SATURATION_THRESHOLD
    ymax = float(ymax)/config.SATURATION_THRESHOLD

    return ymin, ymax


def distill_channel_name(channel):
    """
    Distill a SUS channel name to display only optic, stage, corner info.
    """
    prefix, name = channel.split('-', 1)
    optic, stage, _, _, corner, _ = name.split('_', 5)
    return '{} {} {}'.format(optic, stage, corner)

def gen_channel_names():
    """
    Generates channel names for suspension systems.

    Returns:
        channels = SUS channel names as list of strings
    """
    channel_pattern = "H1:SUS-%s_%s_MASTER_OUT_%s_DQ"

    corners = ['UL','UR','LL','LR']
    triples = ['MC1','MC2','MC3','PRM','PR2','PR3','SRM','SR2','SR3']
    triple_stages = ['M2','M3'] # Only the lower stages, top handled separately
    triple_top_corners = ['T1','T2','T3','LF','RT','SD']
    quads = ['ETMX','ETMY','ITMX','ITMY']
    quad_stages = ['L1','L2','L3'] # Only the lower stages, top handled separately
    quad_top_corners = ['F1','F2','F3','LF','RT','SD']
    channels = []
    shortener = {}
    def add_channel(optic, stage, corner):
        channel_name = channel_pattern % (optic,stage,corner)
        channels.append(channel_name)
        shortener[channel_name] = "%s %s %s" % (optic,stage,corner)

    for optic in quads:
        for corner in quad_top_corners:
            add_channel(optic,'M0',corner)
        for stage in quad_stages:
            for corner in corners:
    	        add_channel(optic,stage,corner)

    for optic in triples:
        for corner in triple_top_corners:
            add_channel(optic,'M1',corner)
        for stage in triple_stages:
            for corner in corners:
                add_channel(optic,stage,corner)

    for corner in ['F1','F2','F3','LF','RT','SD']:
        add_channel('BS','M1',corner)
    for corner in corners:
        add_channel('BS','M2',corner)
    for corner in triple_top_corners:
        add_channel('OMC','M1',corner)

    return channels


def fetch_data(channels, gps, move_left=0, move_right=0):

    mod_window = [config.SAT_SEARCH_WINDOW[0]-move_left, config.SAT_SEARCH_WINDOW[1]+move_right]
    segment = Segment(mod_window).shift(int(gps))
    bufs = data.fetch(channels, segment)
    return bufs, segment

###########################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Create plot/list of saturated suspension channels around a lockloss.
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    find_saturations(event)


if __name__ == '__main__':
    main()
