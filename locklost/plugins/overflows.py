import os
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment, SegmentList
from gwpy.segments import DataQualityFlag, DataQualityDict

from .. import config
from ..event import LocklossEvent, gen_events
from .. import data

#################################################

def find_overflows(event):
    """Create ADC overflow plots centered around the refined lockloss time.

    """
    refined_gps = event.refined_gps
    if not refined_gps:
        logging.warning("overflows being plotted for unrefined lockloss time")
        refined_gps = event.gps

    # set plot rcparams for this plugin
    plt.rcParams['text.usetex'] = False

    logging.info('acquiring ADC overflow data')
    segment = Segment(config.PLOT_WINDOWS['WIDE']).shift(refined_gps)
    all_bufs = data.fetch(generate_all_overflow_channels(), segment)

    # generate plots
    overflows_overview = DataQualityDict()
    for window_type, window in config.PLOT_WINDOWS.items():
        segment = Segment(window).shift(refined_gps)

        for subsystem in config.ADC_OVERFLOWS.keys():

            logging.info('plotting {} ADC overflows for {} subsystem'.format(
                window_type,
                subsystem,
            ))
            for bit1 in range(config.ADC_OVERFLOWS[subsystem]['num_bits']):
                adc_id = config.ADC_OVERFLOWS[subsystem]['ADC_ID']
                channels = generate_overflow_channels(subsystem, bit1)
                bufs = [buf for buf in all_bufs if buf.channel in channels]
                bufs.sort(key = lambda x: x.channel)

                srate = bufs[0].sample_rate
                dt = 1/srate
                t = np.arange(window[0], window[1], dt)

                # find overflows and make a DQ flag for each channel
                overflows = DataQualityDict()
                for buf in bufs:
                    y, t = buf.yt()
                    idx_overflow = np.argwhere(y != 0)
                    t_overflow = t[idx_overflow]
                    overflow_segs = SegmentList([Segment(t, t+dt) for t in t_overflow])
                    _, bit2 = buf.channel.rsplit('_', 1)
                    overflows['bit {}'.format(str(bit2).zfill(2))] = DataQualityFlag(
                        active=overflow_segs,
                        known=SegmentList([segment]),
                    )

                fig = overflows.plot(
                    facecolor='darkred',
                    edgecolor='darkred',
                    known={'alpha': 0.2, 'facecolor': 'lightgray','edgecolor': 'gray'},
                    figsize=[12, 16],
                )

                # set plot settings + title
                fig.tight_layout(pad=0.1)
                fig.subplots_adjust(bottom=0.4, top=0.8)

                ax = fig.gca()
                ax.set_epoch(refined_gps)
                ax.grid(False)
                ax.set_title('ADC Overflows for {} (FEC ID: {}) for ADC number {}'.format(
                    subsystem,
                    adc_id,
                    bit1,
                ))

                # add vertical line to mark refined lock loss time
                ax.axvline(
                    refined_gps,
                    color = 'black',
                    linestyle = '--',
                    lw = 1
                )

                outfile_plot = 'adc_overflow_{}_{}_{}.png'.format(
                    subsystem,
                    bit1,
                    window_type,
                )
                outpath_plot = os.path.join(event.path, outfile_plot)
                fig.savefig(outpath_plot, bbox_inches='tight')
                fig.clf()

                ### aggregate overflows for a specific ADC together
                overflows_overview['{}: ADC number {}'.format(subsystem, bit1)] = overflows.union()

        ### overview overflow plot
        logging.info('plotting {} ADC overflows overview'.format(window_type))
        fig = overflows_overview.plot(
            facecolor='darkred',
            edgecolor='darkred',
            known={'alpha': 0.2, 'facecolor': 'lightgray','edgecolor': 'gray'},
            figsize=[12, 16],
        )

        fig.tight_layout(pad=0.1)
        fig.subplots_adjust(bottom=0.4, top=0.8)

        # set plot settings + title
        ax = fig.gca()
        ax.set_epoch(refined_gps)
        ax.grid(False)
        ax.set_title('ADC Overflows Overview')

        # add vertical line to mark refined lock loss time
        ax.axvline(
            refined_gps,
            color = 'black',
            linestyle = '--',
            lw = 1
        )

        outfile_plot = 'adc_overflow_overview_{}.png'.format(window_type)
        outpath_plot = os.path.join(event.path, outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
        fig.clf()

OVERFLOW_TEMPLATE='{}:FEC-{}_ADC_OVERFLOW_{}_{}'
def generate_overflow_channels(subsystem, bit1):
    ifo = config.IFO
    adc_id = config.ADC_OVERFLOWS[subsystem]['ADC_ID']
    bit_exclude = config.ADC_OVERFLOWS[subsystem]['bit_exclude']

    return [OVERFLOW_TEMPLATE.format(ifo, adc_id, bit1, bit2)
            for bit2 in range(32) if (bit1, bit2) not in bit_exclude]


def generate_all_overflow_channels():
    all_channels = []
    for subsystem in config.ADC_OVERFLOWS.keys():
        for bit1 in range(config.ADC_OVERFLOWS[subsystem]['num_bits']):
            all_channels.extend(generate_overflow_channels(subsystem, bit1))

    return all_channels

############################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Create ADC overflow plots around a lockloss.
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    find_overflows(event)


if __name__ == '__main__':
    main()
