import os
import logging
import argparse
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

from gwpy.segments import Segment

from .. import config
from ..event import LocklossEvent, gen_events
from .. import data

#################################################

def find_lpy(event):
    """Create length, pitch, and yaw plots

    For first saturated channel centered around the refined lockloss time.

    """
    refined_gps = event.refined_gps
    if not refined_gps:
        logging.warning("lpy being plotted for unrefined lockloss time")
        refined_gps = event.gps

    # determine first suspension channel to saturate from saturation plugin
    infile_csv = 'saturations.csv'
    inpath_csv = os.path.join(event.path, infile_csv)
    sat_channel = ''
    if os.path.exists(inpath_csv):
        with open(inpath_csv, 'r') as f:
            first_sat = f.readline()
            if first_sat:
                sat_channel, sat_time = first_sat.split(' ', 1)
    else:
        sat_channel = config.LPY_CHANNELS[0]
        sat_time = None

    # generate the LPY mapping and channels to query from base channel
    lpy_map = channel2lpy_coeffs(sat_channel)
    base_channel, _, _ = sat_channel.rsplit('_', 2)
    channels = [get_sus_channel(base_channel, corner) for corner in lpy_map['corners']]

    for window_type, window in config.PLOT_WINDOWS.items():
        logging.info('Making {} LPY plot for {}'.format(window_type, base_channel))

        segment = Segment(window).shift(refined_gps)
        bufs = data.fetch(channels, segment)

        srate = bufs[0].sample_rate
        t = np.arange(window[0], window[1], 1/srate)

        # calculate LPY using mapping from channels to coeffs
        titles = ['length', 'pitch', 'yaw']
        lpy = defaultdict(lambda: np.zeros(len(bufs[0].data)))
        for title in titles:
            for corner, coeff in lpy_map[title].items():
                idx = channels.index(get_sus_channel(base_channel, corner))
                lpy[title] += bufs[idx].data * coeff

        colors = ['#2c7fb8', '#e66101', '#5e3c99']

        # plot formatting
        plt.rcParams['font.size'] = 28.0
        plt.rcParams['axes.titlesize'] = 28.0
        plt.rcParams['xtick.labelsize'] = 28.0
        plt.rcParams['ytick.labelsize'] = 24.0
        plt.rcParams['legend.fontsize'] = 28.0
        plt.rcParams['agg.path.chunksize'] = config.PLOT_CHUNKSIZE
        fig, axs = plt.subplots(3, sharex=True, figsize=(27,16))

        for ax, dof, color, title in zip(axs, lpy.values(), colors, titles):
            ax.plot(t, dof, color = color, label = title, alpha = 0.8, lw = 2)
            offset = window[0] / 6 # window dependent offset from lock loss
            y_max, y_min = gen_ylim(refined_gps, dof, t, offset = offset)

            # set y-limits manually if channel is flat during this period
            if y_min == y_max:
                ax.set_ylim(-1, 1)
            else:
                ax.set_ylim(y_min, y_max)
            ax.set_xlim(window[0], window[1])
            ax.set_xlabel(
                'Time [s] since refined lock loss at {}'.format(refined_gps),
                labelpad=10,
            )
            ax.grid()
            ax.legend()

        # change y-ticks so that ticks don't overlap
        for ax in axs:
            nbins = len(ax.get_xticklabels())
            locator = MaxNLocator(nbins=nbins, prune='both', min_n_ticks=5)
            ax.yaxis.set_major_locator(locator)

        fig.text(0.08, 0.5, r"Counts", ha='center', va='center', rotation='vertical')
        fig.subplots_adjust(hspace=0)
        plt.setp([ax.get_xticklabels() for ax in fig.axes[:-1]], visible=False)

        # set title
        axs[0].set_title("Length-Pitch-Yaw Plots for {}".format(base_channel))

        outfile_plot = 'lpy_{}.png'.format(window_type)
        outpath_plot = os.path.join(event.path, outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
        fig.clf()


def gen_ylim(refined_gps, y, t, offset = 0):

    t_before = np.where(t < offset)[0]
    y_before = y[t_before]

    ### set range depending on sign
    y_min, y_max = min(y_before), max(y_before)
    if y_min < 0:
        y_min = y_min * 1.2
    else:
        y_min = y_min * 0.8
    if y_max < 0:
        y_max = y_max * 0.8
    else:
        y_max = y_max * 1.2

    return y_max, y_min


def get_sus_channel(base_channel, corner):
    return '{}_{}_DQ'.format(base_channel, corner)


def extract_sus_channel(channel):
    """
    Extract optic, stage, corner info out of a SUS channel name.
    """
    prefix, name = channel.split('-', 1)
    optic, stage, _, _, corner, _ = name.split('_', 5)
    return optic, stage, corner


def channel2lpy_coeffs(channel):
    """
    From a suspension channel, get a dictionary containing the LPY coefficients.
    """
    lpy_map = {}
    optic, stage, corner = extract_sus_channel(channel)

    if optic in ['ETMX', 'ETMY', 'ITMX', 'ITMY']:
        if stage in ['M0']:
            lpy_map['length'] = {'F2': -1, 'F3': -1}
            lpy_map['pitch'] = {'F2': 1, 'F3': 1}
            lpy_map['yaw'] = {'F2': 1, 'F3': -1}
            lpy_map['corners'] = ['F2', 'F3']
        elif stage in ['L1', 'L2', 'L3']:
            lpy_map['length'] = {'UL': 1, 'LL': 1, 'UR': 1, 'LR': 1}
            lpy_map['pitch'] = {'UL': 1, 'LL': -1, 'UR': 1, 'LR': -1}
            lpy_map['yaw'] = {'UL': -1, 'LL': -1, 'UR': 1, 'LR': 1}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['MC1', 'MC2', 'MC3', 'PRM', 'PR2', 'PR3', 'SRM', 'SR2', 'SR3']:
        if stage in ['M1']:
            lpy_map['length'] = {'LF': 1, 'RT': 1}
            lpy_map['pitch'] = {'T2': 1, 'T3': -1}
            lpy_map['yaw'] = {'LF': -1, 'RT': 1}
            lpy_map['corners'] = ['T2', 'T3', 'LF', 'RT']
        elif stage in ['M2', 'M3']:
            lpy_map['length'] = {'UL': 1, 'LL': 1, 'UR': 1, 'LR': 1}
            lpy_map['pitch'] = {'UL': 1, 'LL': -1, 'UR': 1, 'LR': -1}
            lpy_map['yaw'] = {'UL': -1, 'LL': -1, 'UR': 1, 'LR': 1}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['BS']:
        if stage in ['M1']:
            lpy_map['length'] = {'LF': 1, 'RT': 1}
            lpy_map['pitch'] = {'F2': -1, 'F3': -1}
            lpy_map['yaw'] = {'LF': -1, 'RT': 1}
            lpy_map['corners'] = ['F2', 'F3']
        elif stage in ['M2']:
            lpy_map['length'] = {'UL': 1, 'LL': 1, 'UR': 1, 'LR': 1}
            lpy_map['pitch'] = {'UL': 1, 'LL': -1, 'UR': 1, 'LR': -1}
            lpy_map['yaw'] = {'UL': -1, 'LL': -1, 'UR': 1, 'LR': 1}
            lpy_map['corners'] = ['UL', 'LL', 'UR', 'LR']
    elif optic in ['OMC']:
        if stage in ['M1']:
            lpy_map['length'] = {'LF': 1, 'RT': 1}
            lpy_map['pitch'] = {'T2': 1, 'T3': -1}
            lpy_map['yaw'] = {'LF': -1, 'RT': 1}
            lpy_map['corners'] = ['T2', 'T3', 'LF', 'RT']

    return lpy_map

############################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Create plot of ITMX L2 length, pitch, and yaw around a lockloss.
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    find_lpy(event)


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
