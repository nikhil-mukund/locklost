import collections

followups = collections.OrderedDict()
def add_follow(mod):
    followups.update([(mod.__name__, mod)])

from .discover import discover_data
add_follow(discover_data)

from .refine import refine_event
add_follow(refine_event)

from .saturations import find_saturations
add_follow(find_saturations)

from .lpy import find_lpy
add_follow(find_lpy)

from .glitch import analyze_glitches
add_follow(analyze_glitches)

from .overflows import find_overflows
add_follow(find_overflows)

from .state_start import find_lock_start
add_follow(find_lock_start)
