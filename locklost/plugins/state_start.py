import os
import logging
import argparse
import numpy as np

from gwpy.segments import Segment

from .. import config
from ..event import LocklossEvent
from .. import data

##################################################


def find_lock_start(event):
    """Find the start time of the lock loss stretch from the guardian state.

    """
    refined_gps = event.refined_gps
    if not refined_gps:
        logging.warning("lock stretch being calculated with unrefined lockloss time")
        refined_gps = event.gps

    channels = [config.GRD_STATE_N_CHANNEL]

    # find lock stretch
    power = 1
    start_time = None
    window = config.LOCK_STRETCH_WINDOW
    while not start_time:

        # define search window and search for change in guardian state
        segment = Segment(*window).shift(refined_gps)
        logging.info('searching for lock transition in GPS range {} - {}'.format(*segment))
        gbuf = data.fetch(channels, segment)[0]
        gstate, t = gbuf.yt()
        idx = np.argwhere(gstate != event.transition_index[0])
        if len(idx):
            start_idx = np.max(idx)
            start_time = t[start_idx] + (1. / gbuf.sample_rate)

            # ensure that we found start of lock stretch
            if (refined_gps - start_time) > 1:
                logging.info("found start of lock stretch at {:f}".format(start_time))

        window = [edge * (2 ** power) + window[0] for edge in config.LOCK_STRETCH_WINDOW]
        power += 1

    # write start of lock stretch to disk
    path = os.path.join(event.path, 'guard_state_start_gps')
    with open(path, 'w') as f:
        f.write('{:f}\n'.format(start_time))


##################################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Find start of lock stretch
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    find_lock_start(event)


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
