import os
import subprocess
import argparse
import logging
import signal

from gwpy.segments import Segment, SegmentList

from . import __version__
from . import config
from . import search
from . import analyze
from . import online
from . import event
from . import segments

##########

parser = argparse.ArgumentParser(
    prog='locklost',
)
subparser = parser.add_subparsers(
    title='Commands',
    metavar='<command>',
    dest='cmd',
    #help=argparse.SUPPRESS,
)
subparser.required = True

def gen_subparser(cmd, func):
    assert func.__doc__, "empty docstring: {}".format(func)
    help = func.__doc__.split('\n')[0].lower().strip('.')
    desc = func.__doc__.strip()
    p = subparser.add_parser(cmd,
                             formatter_class=argparse.RawDescriptionHelpFormatter,
                             help=help,
                             description=desc)
    p.set_defaults(func=func)
    return p

##########

parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__),
                    help="print version and exit")

p = gen_subparser('search', search.main)
search._parser_add_arguments(p)


p = gen_subparser('analyze', analyze.main)
analyze._parser_add_arguments(p)


p = gen_subparser('online', online.main)
online._parser_add_arguments(p)


def show_event(args):
    """Show event info"""
    e = event.LocklossEvent(args.event)
    print("analyzed: {}".format(e.analyzed))
    print("transition: {}".format(e.transition_index))
    print("nominal time: {}".format(e.gps))
    print("refined time: {}".format(e.refined_gps))
    print("paths:")
    subprocess.call(['find', e.path])

p = gen_subparser('show', show_event)
p.add_argument('event')


def tag_events(args):
    """Tag events by linking to tag directory

    """
    assert args.tag != 'all'
    dname = args.tag
    if args.low_noise:
        dname += '_lownoise'
    tag_dir = os.path.join(config.EVENT_ROOT, dname)
    try:
        os.makedirs(tag_dir)
    except OSError:
        pass
    nlinked = 0
    for e in event.gen_events():
        if args.low_noise:
            if e.transition_index[0] != config.GRD_NOMINAL_STATE[1]:
                continue
        t = e.gps
        if t >= args.start and t < args.end:
            try:
                os.symlink(os.path.join('..', 'all', str(e)),
                           os.path.join(tag_dir, str(e)),
                           )
            except OSError:
                pass
            nlinked += 1
    logging.info("{} events linked to {}".format(nlinked, tag_dir))

# tag_events.__doc__ = tag_events.__doc__.format(config.EVENT_ROOT)
p = gen_subparser('tag', tag_events)
p.add_argument('tag', help="tag name")
p.add_argument('start', type=int, help="start time of events to tag")
p.add_argument('end', type=int, help="end time of events to tag")
p.add_argument('--low-noise', action='store_true', help="only transitions from low noise")


def compress_segments(args):
    """Compress completed segment cache

    """
    segments.compress_segdir(config.SEG_DIR)

p = gen_subparser('compress', compress_segments)


def mask_segment(args):
    """Mask bad segment

    Marks segment as analyzed.

    FIXME: should be tracked separately

    """
    segments.write_segments(
        config.SEG_DIR,
        [Segment(args.start, args.end)],
    )

p = gen_subparser('mask', mask_segment)
p.add_argument('start', type=int, help="segment start")
p.add_argument('end', type=int, help="segment end")

##########

def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(
        level=os.getenv('LOG_LEVEL', 'INFO'),
        format='%(asctime)s %(message)s',
        filename=os.getenv('LOG_FILE'),
    )
    args = parser.parse_args()
    if not config.EVENT_ROOT:
        raise SystemExit("Must specify LOCKLOST_EVENT_ROOT env var.")
    args.func(args)

if __name__ == '__main__':
    main()
