import argparse
import logging
import signal

from gwpy.segments import Segment, SegmentList

from . import config
from . import data
from .event import LocklossEvent
from . import segments
from . import condor

##################################################

def search_buf(buf, previous=None, event_callback=None, dry=True):
    """Search for lock lock events in buffer

    Creates GPS-timestamped event directory in EVENT_DIR, and notes
    state transition index.

    Returns the number of events found in the buffer.

    """
    targets = [s[1] for s in config.GRD_LOCKLOSS_STATES]
    nevents = 0
    for time, pval, val in data.gen_transitions(buf, previous):
        trans = (int(pval), int(val))
        logging.debug("transition: {:0.3f} {}->{}".format(time, trans[0], trans[1]))
        if val not in targets:
            continue
        # yield LocklossEvent(time, trans)
        nevents += 1
        logging.info("lockloss found: {} {}->{}".format(time, *trans))
        if not dry:
            event = LocklossEvent.create(time, trans)
            logging.info("wrote event: {}".format(event.path))
            if event_callback:
                logging.info("executing event callback: {}({})".format(
                    event_callback.__name__, event.gps))
                event_callback(event)
    if not dry:
        segments.write_segments(config.SEG_DIR, [(int(buf.gps_start), int(buf.gps_stop))])
    return nevents


def search(segment, dry=True):
    """Search segment for events

    """
    logging.debug("searching segment {}...".format(segment))
    channel = config.GRD_STATE_N_CHANNEL
    buf = data.fetch([channel], segment)[0]
    nevents = search_buf(buf, dry=dry)
    logging.info("{} events found".format(nevents))
    segments.compress_segdir(config.SEG_DIR)


def search_iterate(segment=None, event_callback=None, dry=True):
    """Iterative search for events (NDS-only)

    """
    if segment:
        logging.info("searching segment {}...".format(segment))
    else:
        logging.info("searching online...")
    previous = None
    nevents = 0
    nbufs = 0
    channel = config.GRD_STATE_N_CHANNEL
    for bufs in data.nds_iterate([channel], start_end=segment):
        buf = bufs[0]
        nevents += search_buf(buf,
                              previous=previous,
                              event_callback=event_callback,
                              dry=dry)
        previous = buf
        nbufs += 1
        # compress segments after 100 buffers
        # FIXME: better way to handle this
        if nbufs % 1000 == 0:
            segments.compress_segdir(config.SEG_DIR)
    # FIXME: if the segment is None and we've broken out of the
    # iteration then there was clearly some problem so we throw an
    # error.  NDS should be throwing an exception here.
    if segment is None:
        raise RuntimeError("NDS iteration returned unexpectedly.")
    else:
        logging.info("{} events found".format(nevents))
        segments.compress_segdir(config.SEG_DIR)

##################################################

def _parser_add_arguments(parser):
    parser.add_argument('start', type=int,
                        help="search start time")
    parser.add_argument('end', type=int,
                        help="search end time")
    sgroup = parser.add_mutually_exclusive_group()
    sgroup.add_argument('--condor', action='store_true',
                        help="create condor submit for search")
    sgroup.add_argument('--iterate', action='store_true',
                        help="iterative offline search (NDS-only)")
    parser.add_argument('--dry', action='store_true',
                        help="dry run (don't write event directories)")


def main(args=None):
    """Search for lockloss events

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    start = int(args.start)
    end = int(args.end)

    assert start < end, "start must be before end"

    full_seg = Segment(start, end)
    completed_segs = segments.load_segments(config.SEG_DIR)
    reduced_segs = SegmentList([full_seg]) - completed_segs

    if args.condor:
        logging.debug("segments: {}".format(reduced_segs))

        def condor_args_gen():
            for s in segments.slice_segments(
                    seglist=reduced_segs,
                    stride=config.SEARCH_STRIDE,
                    overlap=1,
            ):
                yield [
                    ('start', s[0]),
                    ('end', s[1]),
                ]

        condor.create_dag(
            args=['search'],
            submit_args_gen=condor_args_gen,
        )

    elif args.iterate:
        logging.debug("segments: {}".format(reduced_segs))
        for seg in reduced_segs:
            search_iterate(seg, dry=args.dry)

    else:
        logging.debug("segment: {}".format(full_seg))
        search(full_seg, dry=args.dry)


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
