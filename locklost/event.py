import os
import numpy as np

from lal.gpstime import tconvert

from . import config


def _trans_int(trans):
    return tuple(int(i) for i in trans)


class LocklossEvent(object):
    __slots__ = [
        '__path', '__gps', '__transition_index',
        '__utc', '__refined_gps',
    ]

    def __init__(self, gps):
        self.__gps = int(gps)
        self.__path = os.path.join(config.EVENT_DIR, str(self.__gps))
        self.__transition_index = None
        self.__refined_gps = None
        self.__utc = None

    @property
    def gps(self):
        """event GPS time"""
        return self.__gps

    @property
    def utc(self):
        if not self.__utc:
            self.__utc = tconvert(self.gps)
        return self.__utc

    @property
    def path(self):
        """event path"""
        return self.__path

    @property
    def url(self, tag='all'):
        return os.path.join(config.WEB_ROOT, 'events', tag, str(self.gps))

    @property
    def transition_index(self):
        """tuple of indices of Guardian state transition"""
        if not self.__transition_index:
            with open(os.path.join(self.__path, 'transition_index')) as f:
                tis = f.read().split()
            self.__transition_index = _trans_int(tis)
        return self.__transition_index

    def __str__(self):
        return str(self.gps)

    def __repr__(self):
        return '<LocklossEvent: {}, {}->{}>'.format(
            self.gps,
            *self.transition_index)

    #####

    @classmethod
    def create(cls, gps, transition_index):
        """create new event directory"""
        event = cls(gps)
        try:
            os.makedirs(event.path)
        except OSError:
            pass
        with open(os.path.join(event.path, 'guard_state_end_gps'), 'w') as f:
            f.write('{:f}\n'.format(gps))
        with open(os.path.join(event.path, 'transition_index'), 'w') as f:
            f.write('{:.0f} {:.0f}\n'.format(*transition_index))
        return event

    @classmethod
    def from_path(cls, path):
        """load existing event from path"""
        gps = int(os.path.basename(path))
        return cls(gps)

    #####

    @property
    def _aifile(self):
        return os.path.join(self.path, 'analyzing')

    @property
    def _aofile(self):
        return os.path.join(self.path, 'analyzed')

    @property
    def analyzing(self):
        """True if event is being analyzed"""
        return os.path.exists(self._aifile)

    @property
    def analyzed(self):
        """True if event has been analyzed"""
        return os.path.exists(self._aofile)

    @property
    def analysis_version(self):
        """analysis version

        or None if not analyzed

        """
        if os.path.exists(self._aofile):
            path = self._aofile
        elif os.path.exists(self._aifile):
            path = self._aifile
        else:
            return None
        with open(path, 'r') as f:
            return f.readline().strip()

    @property
    def refined(self):
        """True if event has had time refined"""
        return os.path.exists(os.path.join(self.path, 'refined_gps'))

    @property
    def refined_gps(self):
        """refined gps time of event

        Returns gps object, or 'nan' if

        """
        if not self.__refined_gps:
            path = os.path.join(self.path, 'refined_gps')
            if not os.path.exists(path):
                return None
            with open(path) as f:
                gps = f.read().strip()
            if gps in [None, 'nan', float('nan')]:
                self.__refined_gps = float('nan')
            else:
                self.__refined_gps = float(gps)
        if np.isnan(self.__refined_gps):
            return None
        else:
            return self.__refined_gps

    def base_plots_list(self):
        l = []
        for e in os.listdir(self.path):
            if e[-4:] == '.png':
                base, ext = e.split('__')
                if base not in l:
                    l.append(base)
        return l

    def to_dict(self):
        return {
            'id': self.gps,
            'gps': self.gps,
            'utc': self.utc,
            'url': self.url,
            'view_url': os.path.join(config.WEB_ROOT, 'index.cgi?event={}'.format(self.gps)),
            'transition_index': self.transition_index,
            'analyzed': self.analyzed,
            'analysis_version': self.analysis_version,
            'refined': self.refined,
            'refined_gps': self.refined_gps,
        }


def gen_events(event_dir=None):
    if not event_dir:
        event_dir = config.EVENT_DIR
    for event in os.listdir(event_dir):
        try:
            yield LocklossEvent(event)
        except ValueError:
            continue
