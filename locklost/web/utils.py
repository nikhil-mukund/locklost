from datetime import datetime
import glob
import os

from locklost import config
from locklost.event import LocklossEvent

##################################################

def query_to_str(query):
    out_query = ''
    if query.has_key('show') and query['show']:
        out_query += '?show={}'.format(query['show'])
    else:
        out_query += '?show={}'.format(config.QUERY_DEFAULT_SHOW)

    if query.has_key('start') and query['start']:
        out_query += '&start={}'.format(query['start'])
    if query.has_key('end') and query['end']:
        out_query += '&end={}'.format(query['end'])
    if query.has_key('state') and query['state']:
        out_query += '&state={}'.format(query['state'])

    return out_query


def query_to_dict(query):
    if query.has_key('show') and query['show']:
        nshow = int(query['show'])
    else:
        nshow = config.QUERY_DEFAULT_SHOW

    # filter based on time ranges
    if ((query.has_key('start') and query['start']) and
        (query.has_key('end') and query['end'])):
        out_query = {
            'start': int(query['start']),
            'end': int(query['end']),
        }
    elif query.has_key('start') and query['start']:
        out_query = {
            'start': int(query['start']),
            'show': nshow,
        }
    elif query.has_key('end') and query['end']:
        out_query = {
            'end': int(query['end']),
            'show': nshow,
        }
    else:
        out_query = {
            'show': nshow,
        }

    # filter based on guardian state
    if query.has_key('state') and query['state']:
        out_query.update({'state': int(query['state'])})

    return out_query


def list_events(tag='all', **kwargs):
    """
    returns a list of filtered events, sorted by latest
    """
    events = []
    edir = os.path.join(config.EVENT_ROOT, tag)
    for e in sorted(os.listdir(edir)):
        # reject directories that can't be converted to int
        try:
            event = LocklossEvent(e)
            events.append(event)
        except:
            continue

    # filter on state if specified
    if kwargs.has_key('state'):
        events = [event for event in events if event.transition_index[0] == kwargs['state']]

    # filter based on query
    if kwargs.has_key('start') and kwargs.has_key('end'):
        events = [event for event in events if event.gps >= kwargs['start'] and event.gps <= kwargs['end']]
        return sorted(events, reverse=True, key=lambda e: e.gps)

    elif kwargs.has_key('start') and kwargs.has_key('show'):
        events = [event for event in events if event.gps >= kwargs['start']]
        nshow = min(len(events), kwargs['show'])
        return sorted(events[:nshow], reverse=True, key=lambda e: e.gps)

    elif kwargs.has_key('end') and kwargs.has_key('show'):
        events = [event for event in events if event.gps <= kwargs['end']]
        nshow = min(len(events), kwargs['show'])
        return sorted(events[-nshow:], reverse=True, key=lambda e: e.gps)

    elif kwargs.has_key('show'):
        nshow = min(len(events), kwargs['show'])
        return sorted(events[-nshow:], reverse=True, key=lambda e: e.gps)

    else:
        raise ValueError('%s is not a valid kwarg combination for event_list'.format(repr(kwargs)))


def online_status():
    """
    displays HTML-formatted online status information
    """
    cout = os.path.join(config.EVENT_ROOT, 'all', '.condor_online', 'out')
    wcroot = os.path.join(config.WEB_ROOT, 'events', 'all', '.condor_online')
    stat = os.stat(cout)
    dt = datetime.fromtimestamp(stat.st_mtime)
    dtnow = datetime.now()
    age = dtnow - dt
    tsecs = age.total_seconds()
    if tsecs > 3600:
        color = 'red'
    else:
        color = 'green'
    return '<span style="color: {}">online last update: {:0.1f} min ago ({}) [<a href="{}">out</a>, <a href="{}">log</a>]</span>'.format(
        color,
        tsecs/60,
        dt,
        os.path.join(wcroot, 'out'),
        os.path.join(wcroot, 'log'),
    )

def event_plot_urls(event, plot):
    """
    Finds the urls corresponding to an event plot.
    """
    plot_urls = []
    for zoom_level in ['WIDE', 'ZOOM']:
        plot_name = '{}_{}.png'.format(plot, zoom_level)
        plot_path = os.path.join(event.path, plot_name)
        if os.path.exists(plot_path):
            plot_urls.append(os.path.join(event.url, plot_name))

    return plot_urls
