#!/usr/bin/python

import glob
import os

import bottle

from .. import config
from ..event import LocklossEvent
from . import templates, utils

# NOTE: shouldn't be needed, but url paths include this from apache
WEB_SCRIPT = config.WEB_ROOT + '/index.cgi'

##################################################

app = bottle.Bottle()

@app.route("/")
@app.route("/tag/<tag>")
def index(tag='all'):

    if bottle.request.query.format and bottle.request.query.format == 'json':
        query = utils.query_to_str(dict(bottle.request.query))
        bottle.redirect('{}/api/tag/{}{}'.format(WEB_SCRIPT, tag, query))

    else:
        query = utils.query_to_dict(dict(bottle.request.query))

        if query.has_key('show'):
            nshow = query['show']
        else:
            nshow = config.QUERY_DEFAULT_SHOW

        # header
        yield bottle.template(templates.HEADER, web_script=WEB_SCRIPT)

        # online status
        try:
            yield utils.online_status()
        except OSError:
            pass ### no online jobs

        # git info
        yield bottle.template(templates.HEADER_INFO)

        # event/filter forms
        yield bottle.template(templates.EVENT_FORM, web_script=WEB_SCRIPT)
        yield bottle.template(templates.FILTER_FORM, web_script=WEB_SCRIPT)

        # event tags
        ltags = os.listdir(config.EVENT_ROOT)
        yield bottle.template(
             templates.EVENT_TAGS,
             web_script=WEB_SCRIPT,
             ltags=ltags,
             nshow=nshow,
        )

        # query info
        yield '<h3>tag: "{}"'.format(tag)
        for query_key, query_val in query.items():
            yield ', {}: {}'.format(query_key, query_val)
        yield '</h3>'

        # event table header
        yield bottle.template(templates.EVENT_TABLE_HEADER, tag=tag, nshow=nshow)

        # event table row
        for i, event in enumerate(utils.list_events(tag, **query)):
            if event.analyzed:
                fflag = "analyzed [{}]".format(event.analysis_version)
                state = 'success'
            elif event.analyzing:
                fflag = "analyzing"
                state = 'warning'
            else:
                fflag = "fail"
                state = 'danger'
            analyze_out_link = os.path.join(event.url, 'condor_analyze', 'out')

            yield bottle.template(
                templates.EVENT_ROW,
                web_script=WEB_SCRIPT,
                state=state,
                previous=event.transition_index[0],
                lockloss=event.transition_index[1],
                gps=event.gps,
                utc=event.utc,
                analyze_out_link=analyze_out_link,
                fflag=fflag,
            )

        # footers
        yield bottle.template(templates.EVENT_TABLE_FOOTER)
        yield bottle.template(templates.FOOTER)


@app.route("/event/<gps>")
def event_summary(gps):
    event = LocklossEvent(gps)

    # header
    yield bottle.template(templates.HEADER, web_script=WEB_SCRIPT)

    # online status
    try:
        yield utils.online_status()
    except OSError:
        pass ### no online jobs

    # git info
    yield bottle.template(templates.HEADER_INFO)

    # event form
    yield bottle.template(templates.EVENT_FORM, web_script=WEB_SCRIPT)
    yield bottle.template(templates.FILTER_FORM, web_script=WEB_SCRIPT)

    # event tags
    ltags = os.listdir(config.EVENT_ROOT)
    yield bottle.template(
         templates.EVENT_TAGS,
         web_script=WEB_SCRIPT,
         ltags=ltags,
         nshow=config.QUERY_DEFAULT_SHOW,
    )

    # event summary
    yield '<h3>lockloss: <a href="{}">{}</a></h3>'.format(
        event.url,
        event.gps,
    )
    p, l = event.transition_index
    yield '<p>state transition (<b><tt>{node}</tt></b>): <b>{prev} -> {lockloss}</b></p>'.format(
        node=config.GRD_NODE,
        prev=p,
        lockloss=l,
    )
    yield '<p>event time: {}</p>'.format(event.utc)

    ### indicator plot
    yield '<div class="container">'
    yield '<div class="row">'
    for plot_url in utils.event_plot_urls(event, 'indicators'):
        yield bottle.template(templates.IMAGE, url=plot_url, size=6)
    yield '</div>'
    yield '</div>'
    gps_path = os.path.join(event.path, 'refined_gps')
    if os.path.exists(gps_path):
        with open(gps_path, 'r') as f:
            yield '<p><b>Refined GPS:</b> {}</p>'.format(f.readline())

    ### saturation plots
    yield '<hr /><div class="container">'
    yield '<h3>Saturation Plots</h3><br />'
    yield '<div class="row">'
    csv_path = os.path.join(event.path, 'saturations.csv')
    if os.path.exists(csv_path):
        ### link plots
        for plot_url in utils.event_plot_urls(event, 'saturations'):
            yield bottle.template(templates.IMAGE, url=plot_url, size=6)

        yield bottle.template(templates.SATURATION_TABLE_HEADER)

        ### saturation table
        with open(csv_path, 'r') as f:
            for line in f:
                sat_channel, sat_time = line.split()
                yield bottle.template(
                    templates.SATURATION_TABLE_ROW,
                    channel=sat_channel,
                    time=sat_time,
                )

        yield bottle.template(templates.SATURATION_TABLE_FOOTER)
    else:
        yield '<p>No saturations found.</p>'
    yield '</div>'
    yield '</div>'

    ### LPY plots
    yield '<hr /><div class="container">'
    yield '<h3>Length-Pitch-Yaw Plots</h3><br />'
    yield '<p>DAC counts for first suspension stage to saturate</p>'
    yield '<div class="row">'
    for plot_url in utils.event_plot_urls(event, 'lpy'):
        yield bottle.template(templates.IMAGE, url=plot_url, size=6)
    yield '</div>'
    yield '</div>'

    ### ADC overflow plots
    yield '<hr /><div class="container">'
    yield '<h3>ADC Overflow Plots</h3><br />'
    yield '<h5>Overview</h5>'

    # overview plots
    yield '<div class="row">'
    for plot_url in utils.event_plot_urls(event, 'adc_overflow_overview'):
        yield bottle.template(templates.IMAGE, url=plot_url, size=6)
    yield '</div>'

    # specific ADC plots
    for ii, subsystem in enumerate(['ASC', 'LSC', 'OMC']):
        adc_plots = sorted(glob.glob(os.path.join(event.path, 'adc_overflow_{}_*.png'.format(subsystem))))

        yield bottle.template(templates.ADC_OVERFLOW_HEADER, idx=ii, subsystem=subsystem)
        yield '<div class="row">'
        for adc_plot in adc_plots:
            adc_name = os.path.basename(adc_plot)
            sat_url = os.path.join(event.url, adc_name)
            yield bottle.template(templates.IMAGE, url=sat_url, size=6)
        yield '</div>'
        yield bottle.template(templates.ADC_OVERFLOW_FOOTER)

    # footers
    yield bottle.template(templates.FOOTER)


@app.route("/api/tag/<tag>")
def get_events(tag='all'):
    query = utils.query_to_dict(dict(bottle.request.query))
    events = [event.to_dict() for event in utils.list_events(tag, **query)]
    return {'events': events}


@app.route('/show_event', method='POST')
def show_event():
    gps = bottle.request.forms.get('event')
    bottle.redirect('{}/event/{}'.format(WEB_SCRIPT, gps))


@app.route('/filter_events', method='POST')
def filter_events():
    query = utils.query_to_str(dict(bottle.request.forms))
    bottle.redirect('{}{}'.format(WEB_SCRIPT, query))


##################################################

if __name__ == '__main__':
    bottle.run(app, server='cgi', debug=True)
