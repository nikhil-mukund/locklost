HEADER = """
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>H1 lock losses</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css" media="screen">
</head>
<body>
<div class="container">
<h2></h2>
<h2><a href="{{web_script}}">H1 lock losses</a></h2>
"""

HEADER_INFO = """
<br />
<a href="https://git.ligo.org/jameson.rollins/locklost">git repo </a>
<a href="https://git.ligo.org/jameson.rollins/locklost/blob/master/README.md">docs </a>
<a href="https://git.ligo.org/jameson.rollins/locklost/issues">issue tracker </a>
"""

FOOTER = """
</div>
<br />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
"""

EVENT_FORM = """
<hr />
<div class="container">
<form method="post" action="{{web_script}}/show_event">
 <div class="row">
 show event:
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="GPS time" name="event">
   </div>
 </div>
</form>
</div>
"""

FILTER_FORM = """
<br />
<div class="container">
<form method="post" action="{{web_script}}/filter_events">
 <div class="row">
 filter events:
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="GPS start" name="start">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="GPS end" name="end">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="# of events" name="show">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="Guardian state" name="state">
   </div>
   <div class="col-sm-2">
     <button type="submit" class="btn btn-primary mb-2">Submit</button>
   </div>
 </div>
</form>
</div>
"""

EVENT_TAGS = """
<br />
<p>tags: 
% for ltag in ltags:
    <a href="{{web_script}}/tag/{{ltag}}?show={{nshow}}"> {{ltag}}</a>
% end
</p>
<hr />
"""

IMAGE = """
<div class="col-md-{{size}}">
<div class="thumbnail">
<a href="{{url}}" target="_blank"><img src="{{url}}" style="width:100%" class="img-responsive" alt="" /></a>
</div>
</div>
<br />
"""

EVENT_TABLE_HEADER = """
<div class="container">
<div class="col-md-12">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th>GPS</th>
<th>UTC</th>
<th>last state</th>
<th>lockloss state</th>
<th>followup</th>
</tr>
</thead>
<tbody>
"""

EVENT_ROW = """
<tr>
<div class="btn-group">
<td><a href={{web_script}}/event/{{gps}}>{{gps}}</a></td>
<td><a href={{web_script}}/event/{{gps}}>{{utc}}</td>
<td>{{previous}}</td>
<td>{{lockloss}}</td>
<td><a href="{{analyze_out_link}}" class="btn-sm btn-{{state}} btn-block" role="button" style="text-align:center;">{{fflag}}</a></td>
</div>
</tr>
"""

EVENT_TABLE_FOOTER = """
</tbody>
</table>
</div>
</div>
"""

SATURATION_TABLE_HEADER = """
<div class="panel-group">
<div class="panel panel-default">
<div class="panel-heading">
<h5 class="panel-title"><a data-toggle="collapse" href="#sat1">Saturations table by channel (click to show)</a></h5>
</div>
<div id="sat1" class="panel-collapse collapse">
<div class="panel-body">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th>Channel</th>
<th>Time to first saturation</th>
</tr>
</thead>
<tbody>
"""

SATURATION_TABLE_FOOTER = """
</tbody>
</table>
</div>
</div>
</div>
</div>
"""

ADC_OVERFLOW_HEADER = """
<br />
<div class="panel-group">
<div class="panel panel-default">
<div class="panel-heading">
<h5 class="panel-title"><a data-toggle="collapse" href="#adc{{idx}}">All {{subsystem}} overflows (click to show)</a></h5>
</div>
<div id="adc{{idx}}" class="panel-collapse collapse">
<div class="panel-body">
"""

SATURATION_TABLE_ROW = """
<tr>
<div class="row">
<td>{{channel}}</td>
<td>{{time}}</td>
</div>
</tr>
"""

ADC_OVERFLOW_FOOTER = """
</div>
</div>
</div>
</div>
"""
