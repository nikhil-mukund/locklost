import os
import traceback
import argparse
import logging
import signal

from . import __version__
from . import config
from .plugins import followups
from .event import LocklossEvent, gen_events
from . import condor

##################################################

def analyze_event(event):
    """Execute all follow-up plugins for this event.

    """
    assert not os.path.exists(event._aifile)
    assert not os.path.exists(event._aofile)
    logging.info("analysis version: {}".format(__version__))
    with open(event._aifile, 'w') as f:
        f.write(__version__+'\n')
    complete = True
    for name, func in followups.items():
        logging.info("executing followup: {}({})".format(
            name, event.gps))
        try:
            func(event)
        except:
            complete = False
            traceback.print_exc()
    if complete:
        os.rename(event._aifile, event._aofile)
        logging.info("analysis complete")
    else:
        logging.warning("INCOMPLETE ANALYSIS")
        os.remove(event._aifile)


def analyze_condor(event):
    if event.analyzing:
        logging.warning("event analysis in progress, aborting")
        return
    condor_dir = os.path.join(event.path, 'condor_analyze')
    try:
        os.makedirs(condor_dir)
    except:
        pass
    sub = condor.CondorSubmit(
        condor_dir,
        ['analyze', str(event.gps)],
        local=False,
    )
    sub.write()
    sub.submit()

##################################################

def _parser_add_arguments(parser):
    egroup = parser.add_mutually_exclusive_group(required=True)
    egroup.add_argument('event', type=int, nargs='?',
                        help="event ID / GPS second")
    egroup.add_argument('--all', action='store_true',
                        help="create condor job to analyze all un-analyzed events")
    parser.add_argument('--condor', action='store_true',
                        help="condor analyze event")
    parser.add_argument('--rerun', action='store_true',
                        help="re-analyze event(s)")


def main(args=None):
    """Analyze event

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    if args.all:
        # FIXME: better indicator of completed analysis other than
        # presence of "refined"
        logging.info("finding un-analyzed events...")
        all_events = [e for e in gen_events()
                      if not (e.analyzed or e.analyzing) or args.rerun]
        logging.info("{} events to analyze...".format(len(all_events)))

        def condor_args_gen():
            for e in all_events:
                yield [('event', e)]

        condor.create_dag(
            args=['analyze'],
            submit_args_gen=condor_args_gen,
        )

    else:
        if os.getenv('FAKE'):
            try:
                os.makedirs(os.path.join(config.EVENT_DIR, str(args.event)))
            except OSError:
                pass

        event = LocklossEvent(args.event)

        if event.analyzing:
            raise SystemExit("Event already being analyzed")
        if event.analyzed and not args.rerun:
            raise SystemExit("Event already analyzed (--rerun to rerun)")
        if os.path.exists(event._aofile):
            os.remove(event._aofile)

        if args.condor:
            analyze_condor(event)
        else:
            analyze_event(event)


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
