import os
import sys
import shutil
import collections
import subprocess
# import uuid
import logging

from . import config


def _write_executable(path,
                      data_access='gwpy',
                      log_file='',
):
    with open(path, 'w') as f:
        f.write("""#!/bin/sh
export IFO={}
export LOCKLOST_EVENT_ROOT={}
export PYTHONPATH={}
export DATA_ACCESS={}
export NDSSERVER={}
export LIGO_DATAFIND_SERVER={}
export LOG_FILE={}
export CONDOR_ACCOUNTING_GROUP={}
export CONDOR_ACCOUNTING_GROUP_USER={}
{} -m locklost "$@" 2>&1
""".format(config.IFO,
           config.EVENT_ROOT,
           os.getenv('PYTHONPATH', ''),
           data_access,
           os.getenv('NDSSERVER', ''),
           os.getenv('LIGO_DATAFIND_SERVER'),
           log_file,
           config.CONDOR_ACCOUNTING_GROUP,
           config.CONDOR_ACCOUNTING_GROUP_USER,
           sys.executable,
    ))
    os.chmod(path, 0755)


class CondorDAG(object):

    def __init__(self, condor_dir, args, submit_args_gen):
        """Create HTCondor DAG

        `args` are common command line arguments to the locklost call.
        `submit_args_gen` is a generator of command line arguments as
        a list of (key, value) tuples.

        The '<condor_dir>' directory will be created with the
        following components:

          <condor_dir>/.condor/dag     condor DAG file
          <condor_dir>/.condor/submit  condor submit file
          <condor_dir>/.condor/exec    locklost execution script
          <condor_dir>/.condor/logs/   log file directory

        """
        self.condor_dir = os.path.abspath(condor_dir)
        self.submit_args_gen = submit_args_gen

        # extract the argument names from the submit arg generator
        for sargs in self.submit_args_gen():
            sargvars = ['$({})'.format(arg[0]) for arg in sargs]
            break
        args += sargvars
        self.sub = CondorSubmit(
            self.condor_dir,
            args,
            log='logs/$(jobid)_$(cluster)_$(process).',
        )

        self.dag_path = os.path.join(self.condor_dir, 'dag')

    # def __gen_job_id__(self):
    #     return str(uuid.uuid4())

    def __gen_VARS(self, *args):
        return ['{}="{}"'.format(k, v) for k, v in args]

    def as_string(self):
        s = ''
        for jid, sargs in enumerate(self.submit_args_gen()):
            VARS = self.__gen_VARS(('jobid', jid), *sargs)
            if jid == 0:
                job_string = '''
JOB   {jid} {sub}
VARS  {jid} {VARS}
RETRY {jid} 1
'''
            else:
                job_string = '''
JOB   {jid} {sub}
VARS  {jid} {VARS}
PARENT 0 CHILD {jid}
RETRY {jid} 1
'''
            s += job_string.format(jid=jid,
                                   sub=self.sub.sub_path,
                                   VARS=' '.join(VARS),
                                   )
        logging.info("condor DAG {} jobs".format(jid+1))
        return s

    def write(self):
        os.makedirs(os.path.join(self.condor_dir, 'logs'))
        self.sub.write()
        with open(self.dag_path, 'w') as f:
            f.write(self.as_string())

    def submit(self):
        assert os.path.exists(self.dag_path), "Must write() before submitting"
        logging.info("condor submit dag: {}".format(self.dag_path))
        subprocess.call(['condor_submit_dag', self.dag_path])


class CondorSubmit(object):

    def __init__(self, condor_dir, args, log='', local=False, restart=False, notify_user=None):
        """Generate HTCondor submit file

        `args` is list of string argument names.

        """
        assert config.CONDOR_ACCOUNTING_GROUP
        assert config.CONDOR_ACCOUNTING_GROUP_USER
        self.condor_dir = os.path.abspath(condor_dir)
        self.sub_path = os.path.join(condor_dir, 'submit')
        if local:
            universe = 'local'
        else:
            universe = 'vanilla'
        if restart:
            on_exit_remove = '(ExitBySignal == False) && (ExitCode == 0)'
        else:
            on_exit_remove = 'True'
        submit = [
            ('executable', '{}/exec'.format(self.condor_dir)),
            ('arguments', ' '.join(args)),
            ('universe', universe),
            ('accounting_group', config.CONDOR_ACCOUNTING_GROUP),
            ('accounting_group_user', config.CONDOR_ACCOUNTING_GROUP_USER),
            ('getenv', 'True'),
            ('output', '{}/{}out'.format(self.condor_dir, log)),
            ('log', '{}/{}log'.format(self.condor_dir, log)),
            ('on_exit_remove', on_exit_remove),
            ('periodic_release', 'True'),
        ]
        if notify_user:
            submit.append(('notification', 'Always'))
            submit.append(('notify_user', notify_user))
        self.submit_dict = collections.OrderedDict(submit)

    def write(self):
        _write_executable(os.path.join(self.condor_dir, 'exec'))
        with open(self.sub_path, 'w') as f:
            for key, val in self.submit_dict.items():
                f.write('{} = {}\n'.format(key, val))
            f.write('queue 1\n')

    def submit(self):
        assert os.path.exists(self.sub_path), "Must write() before submitting"
        logging.info("condor submit: {}".format(self.sub_path))
        subprocess.call(['condor_submit', self.sub_path])

##################################################

def create_dag(args, submit_args_gen):
    cmd = args[0]
    condor_dir = os.path.join(config.EVENT_DIR, '.condor_'+cmd)
    if os.path.exists(os.path.join(condor_dir, 'dag.lock')):
        raise SystemExit("condor job running: {}".format(condor_dir))
    os.makedirs(condor_dir)
    dag = CondorDAG(
        condor_dir,
        args,
        submit_args_gen,
    )
    dag.write()
    dag.submit()
    print("""
Run the following to monitor condor job:
$ watch condor_q -nobatch
$ tail -F {0}.*
""".format(dag.dag_path).strip())
    return dag
