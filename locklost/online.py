import os
import argparse
import logging
import signal

from . import config
from . import search
from . import analyze
from . import condor

##################################################

def _parser_add_arguments(parser):
    parser.add_argument('--dry', action='store_true',
                        help="dry run (don't write event directories)")
    parser.add_argument('--condor', action='store_true',
                        help="launch condor-managed online search")
    parser.add_argument('--force', action='store_true',
                        help="force launch condor")


def main(args=None):
    """Online search for lockloss events

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    if args.condor:
        condor_dir = os.path.join(config.EVENT_DIR, '.condor_online')

        try:
            os.makedirs(condor_dir)
        except OSError:
            if not args.force:
                raise

        sub = condor.CondorSubmit(
            condor_dir,
            ['online'],
            local=True,
            restart=True,
            notify_user=os.getenv('CONDOR_NOTIFY_USER'),
        )
        sub.write()
        sub.submit()
        print("""
Run the following to monitor:
watch condor_q -nobatch
tail -F {0}/{{log,out}}
""".format(condor_dir).strip())

    else:
        search.search_iterate(
            event_callback=analyze.analyze_condor,
            dry=args.dry)


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
